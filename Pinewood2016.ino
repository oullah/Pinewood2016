#include <FastLED.h>

#define DATA_PIN 6
#define NUM_LEDS 18

CRGB leds[NUM_LEDS];
const CHSV BLUE = CHSV(160,255,255);
const CHSV WHITE = CHSV(0, 0, 255);
const CHSV BLACK = CHSV(0, 0, 0);

void setup()
{
	FastLED.addLeds<NEOPIXEL, DATA_PIN>(leds, NUM_LEDS);
	FastLED.setBrightness(255);
	set_max_power_in_volts_and_milliamps(5, 500);
}

void loop()
{
	int blinks[] = { 0, 15 };
	EVERY_N_MILLISECONDS(100) {
		//blink(blinks);
	}
	EVERY_N_MILLISECONDS(200) {
		dance(0, 16, BLUE, BLACK);
	}
	EVERY_N_MILLISECONDS(50) {
		flicker_tails();
	}
	show_at_max_brightness_for_power();
}

void blink(int blinks[]) {
	static bool blink = false;
	for (int i = 0; i < sizeof(blinks); i++) {
		if (!blink) {
			leds[blinks[i]] = BLACK;
		}
		else {
			leds[blinks[i]] = BLUE;
		}
	}
	blink = !blink;
}

void dance(int start, int end, CHSV color1, CHSV color2) {
	static bool flash = false;

	for (int i = start; i < end; i++) {
		if ((i % 2 == 0 && flash) || (i % 2 > 0 && !flash)) {
			leds[i] = color1;
		}
		else {
			leds[i] = color2;
		}
	}
	flash = !flash;
}

void flicker_tails() {
	static int tail_v = 100;
	static bool increase = true;
	const int V_MIN = 120;
	int tails[] = { 16, 17 };

	tail_v = increase ? tail_v + 10 : tail_v - 2;

	if (tail_v >= 255) {
		increase = false;
		tail_v = 255;
	}
	if (tail_v <= V_MIN) {
		increase = true;
		tail_v = V_MIN;
	}

	for (int i = 0; i < sizeof(tails); i++) {
		leds[tails[i]] = CHSV(0, 255, tail_v);
	}
}
